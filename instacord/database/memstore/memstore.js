const logger = new (require('../../util/logger'))("Memstore backend");
const GenericStore = require('../generic');

class Memstore extends GenericStore  {
  /**
   * Constructs a new Memstore backend, which just keeps everything
   * in a big hashtable/object
   * @emits ready
   */
  constructor(url, dbName) {
    super();
    this.users = {};
    this.guilds = {};
    this.channels = {};
    this.emit('ready');
  }

  /**
   * Returns the underlying DB wrapper, or null.
   * @return {Object}
   */
  getDatabase() {
    return this;
  }

  getUser(id) {
    this.users[id] = this.users[id] || {};
    this.users[id].save = function() {};
    return Promise.resolve(this.users[id]);
  }

  getGuild(id) {
    this.guilds[id] = this.guilds[id] || {};
    this.guilds[id].save = function() {};
    return Promise.resolve(this.guilds[id]);
  }

  getChannel(id) {
    this.channels[id] = this.channels[id] || {};
    this.channels[id].save = function() {};
    return Promise.resolve(this.channels[id]);
  }
}

module.exports = Memstore;
