const RocksDB = require('../../util/tryrequire')('level-rocksdb');
const logger = new (require('../../util/logger'))("RocksDB backend");
const GenericStore = require('../generic');

class RocksBackend extends GenericStore  {
  /**
   * Constructs a new RocksDB backend
   * @emits ready
   */
  constructor(folder) {
    super();
    if (!RocksDB) throw new Error("Please install RocksDB to use this backend (npm install level-rocksdb --save).");
    this.db = RocksDB(folder);
    this.emit('ready');
  }

  /**
   * Returns the underlying DB wrapper, or null.
   * @return {Object}
   */
  getDatabase() {
    return this;
  }

  /**
   * Gets a value from the database
   * @param {String} key the value to get
   * @return {Promise} resolves with the key's value
   */
  get(key) {
    return new Promise((resolve, reject) => {
      this.db.get(key, (err, value) => {
        if (err && err.name == 'NotFoundError')
          return resolve(null);
        if (err) return reject(err);
        resolve(JSON.parse(value));
      });
    });
  }


  /**
   * Sets a value in the database
   * @param {String} key the key to set the value as
   * @param {Object} value the value to set
   * @return {Promise}
   */
  put(key, value) {
    return new Promise((resolve, reject) => {
      this.db.put(key, JSON.stringify(value), (err) => {
        if (err) return reject(err);
        resolve();
      });
    });
  }

  /**
   * Alias for put
   * @param {String} key
   * @param {Object} value
   * @return {Promise}
   */
  set(key, value) {
    return this.put(key, value);
  }

  async getUser(id) {
    var value = (await this.get('user-'+id)) || {};
    value.save = (() => this.put('user-'+id, value));
    return value;
  }

  async getGuild(id) {
    var value = (await this.get('guild-'+id)) || {};
    value.save = (() => this.put('guild-'+id, value));
    return value;
  }

  async getChannel(id) {
    var value = (await this.get('channel-'+id)) || {};
    value.save = (() => this.put('channel-'+id, value));
    return value;
  }
}

module.exports = RocksBackend;
