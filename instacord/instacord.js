const Router = require('./router');
const Logger = require('./util/logger');
const logger = new Logger("instacord");

const Discord = require('discord.js');

const MongoBackend = require('./database/mongo/mongo');
const RocksBackend = require('./database/rocks/rocks');
const Memstore = require('./database/memstore/memstore');

class Instacord extends Router {
  constructor() {
    super();
    /**
     * If this flag is set, user/channel/guild config data is loaded from the
     * database immediately when any message is processed by the bot. You can
     * disable this manually to prevent database access.
     * This flag is disabled when you use .load() to load from the database.
     * @type {Boolean}
     */
    this.loadImmediately = true;
  }

  /**
   * @return {class} Router
   */
  static get Router() {
    return Router;
  }

  /**
   * Finalizes the Instacord instance, logging in the selected backend
   * and performing database connections. Further changes to backend are
   * unsupported after this is called.
   * @param {Client|string} token The bot token or discord.js instance
   * @returns {Promise<string>} Token of the discord account used
   */
  login(token) {
    if (!token) throw new Error("Expected a bot token as first parameter");
    if (!this.databaseBackend) {
      logger.warn("No database backend chosen, defaulting to memstore");
      logger.warn("Choose a backend using .mongodb(<url>) or .memstore() or .rocksdb(<folder>)");
      this.databaseBackend = Memstore;
    }
    if (this.databaseBackend == Memstore) {
      logger.warn("Using in-memory memstore as a database backend.");
      logger.warn("!All data will be lost when the bot is stopped!");
    }

    if (token instanceof Discord.Client) {
      var client = token;
    } else {
      var client = new Discord.Client();
      client.login(token);
    }

    /**
     * The underlying client object provided by discord.js
     * @type {Client}
     */
    this.client = client;
    this.client.on('message', (message) => this._processMessage(message));
    this.client.on('ready', () => logger.debug("Discord.js client ready"));

    /**
     * The underlying db wrapper provided by a database backend.
     * @type {Object}
     * @emits ready when the database is ready
     */
    this.db = new (this.databaseBackend)();
    this.db.on('ready', () => {
      logger.debug("Database ready");
    });

    return this.client.login(token);
  }

  /**
   * Returns the selected datbase wrapper. The actual db client is *usually* on
   * the returned object's 'getDatabase' method, but this will vary by backend.
   * @return {DatabaseBackend}
   */
  getDatabaseWrapper() {
    return this.db;
  }

  /**
   * Adds middleware which loads user, guild and channel config data
   * from the database. You should put this after a prefix so that you
   * aren't loading from the database for every single message sent,
   * even those unrelated to your bot.
   * If this is not called, database keys will be loaded immediately,
   * for every single message.
   * The loaded data is put into message.storage
   * message.storage = { user: <data>, channel: <data>, guild: <data> }
   * @return {function} middleware which you should plug into a route handler
   * @example instacord.group('<PREFIX>', instacord.load())
   */
  load() {
    this.loadImmediately = false;
    return ((sub, message, actions) => {
      return this._doLoad(message, ([user, channel, guild]) => {
        message.storage = message.storage || {};
        Object.assign(message.storage, {
          user: user,
          channel: channel,
          guild: guild
        });
        return new Promise(resolve => actions.postExecute(resolve));
      }).catch((ex) => {
        logger.error("Failed to load data from database:", ex);
      })
    });
  }

  /**
   * Loads user, channel and guild data for a message from the database backend.
   * @param {Object} message the message object
   * @param {Function} callback the callback to be called with the retrieved
   *   data, first param is [user, guild, channel]. Data is re-saved to database
   *   after a promise returned from the callback resolves. You can reject the
   *   promise to abort the save process
   * @param {Boolean} immediate if the load handler is being executed in an
   *   immediate context (i.e. immediately after recieving a message). If this
   *   is true and .loadImmediately is false, the _doLoad operation is aborted.
   */
  _doLoad(message, callback, immediate) {
    if (immediate && !this.loadImmediately) return callback([null, null, null]);
    return Promise.all([
      this.db.getUser(message.author.id),
      this.db.getGuild(message.guild.id),
      this.db.getChannel(message.channel.id)
    ]).then(([user, guild, channel]) => {
      Promise.resolve().then(() => {
        return callback([user, channel, guild]);
      }).then(() => {
        if (user && user.save) user.save();
        if (guild && guild.save) guild.save();
        if (channel && channel.save) channel.save();
      }).catch(ex => {
        logger.debug("Aborting save to database, promise rejected: ", ex);
      })
    });
  }

  /**
   * Sends a fake message through the client handler.
   * No client or underlying message object will be availible. Only the
   * instacord-defined APIs will exist on the message and actions objects.
   * @param {string} messageText the message content.
   * @return {Promise} A promise that resolves after the message executes.
   */
  fakeMessage(messageText) {
    return this._processMessage({
      content: messageText,
      author: {
        id: -1,
        username: "Test user",
        discriminator: "0001",
        toString: () => "@Test user#0001"
      },
      channel: {
        id: -1
      },
      guild: {
        id: -1
      },
      respond: (text) => logger.info(text),
      reply: (text) => logger.info(text)
    });
  }

  /**
   * Begins resolution of a message. Use fakeMessage to properly construct a
   * message object for testing.
   *
   * The function signature passed to the Router (cmd, group, etc.):
   *  {String} the remaining unparsed message substring
   *  {Object} the instacord message object
   *  {Object} the 'actions' object, which contains:
   *    {function} reply({String}): Reply to the message (@mention the user)
   *    {function} respond({String}): Sends message in the same channel
   *    {function} postExecute({Func}): Add a post-message-processing callback
   *
   * @param {Object} message the message object
   * @return {Promise} a promise that resolves after processing is done.
   */
  _processMessage(message) {
    return this._doLoad(message, ([user, channel, guild]) => {
      message.storage = {
        user: user,
        channel: channel,
        guild: guild
      };

      var actions = {
        postExecute: function(handler) {
          this._postExecuteQueue = this._postExecuteQueue || [];
          this._postExecuteQueue.push(handler);
        }
      };

      return this.resolve(message.content, message, actions).then(() => {
        if (actions._postExecuteQueue) {
          logger.debug("Execute post-execute queue");
          actions._postExecuteQueue.forEach(handler => {
            handler(message.content, message, actions)
          });
        }
      });
    }, true);
  }

  /**
   * Registers an on ready callback which is fired when the bot is logged in.
   * @param {Function} callback
   * @return {Instacord} this
   */
  ready(callback) {
    if (!this.client)
      throw new Error("Please call .login() before registering ready callback");
    this.client.on('ready', callback);
    return this;
  }

  /**
   * Selects MongoDB as the database backend
   * @param {string} server The MongoDB server url
   * @param {string} [database] The database name
   * @return {Instacord} this
   */
  mongodb(server, database) {
    if (!server) throw new Error("Expected server URL as first argument");
    if (!database)
      logger.warn("No MongoDB database specified, using default db 'instacord'."
                + " Pass database as second argument to .mongodb(<mongourl>)");
    database = database || "instacord";
    this.databaseBackend = (function() {
      return new MongoBackend(server, database);
    });
    return this;
  }

  /**
   * Returns the MongoDB client used internally by instacord
   * Only applicable when using the mongodb backend and connected.
   * @return {MongoClient} the mongo client
   */
  mongoClient() {
    if (!this.db) return;
    return this.db.client;
  }

  /**
   * Selects RocksDB as the database backend
   * @param {string} dbfolder The folder to store the database in
   * @return {Instacord} this
   */
  rocksdb(dbfolder) {
    dbfolder = dbfolder || './rocksdb';
    this.databaseBackend = (function() {
      return new RocksBackend(dbfolder);
    });
    return this;
  }

  /**
   * Selects Memstore as the database backend
   * @return {Instacord} this
   */
  memstore() {
    this.databaseBackend = Memstore;
    return this;
  }
}

module.exports = Instacord;
