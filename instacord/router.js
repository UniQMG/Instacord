const EventEmitter = require('events').EventEmitter;
const Logger = require('./util/logger');
const Command = require('./command');
const logger = new Logger("router");
const util = require('util');

/**
 *
 *
 */
class Router {
  /**
   * Constructs a new Router, used for routing commands.
   * You probably shouldn't make one of these directly unless you're mounting
   * it express-style
   */
  constructor() {
    this.mounts = new Map();
  }


  /**
   * Generic router middleware. Subclasses of router should define their own
   * implementation of this interface when they add custom parameters.
   * @callback middleware
   * @param {string} sub The unparsed substring of the command
   * @returns {?(Promise|boolean)} Reject the promise or return a falsey value
   *   to discontinue deeper (but not wider) execution.
   */

  /**
   * @callback ResolverFunction
   * @param {string} cmd The partial user command to parse
   * @return {(string|number|null)} the characters or number of characters
   *   to consume, or false to cancel
   */

   /**
    * @typedef {function} Middleware
    * @param {string} cmd The unparsed portion of the user command
    * @return {promise|false} Command execution will wait for a returned
    *   promise to resolve or reject. rejected promises prevent further
    *   middleware from executing.
    */

  /**
   * Constructs a new Command on the given path.
   * @param {Resolver} cmd The comparator to test user commands against
   * @param {...Middleware} middleware The middleware functions to execute
   * @return {Router} this
   */
  onCommand(cmd, ...middleware) {
    var command = new Command(cmd);
    middleware.forEach(ware => command.chain(ware));
    this.mounts.set(cmd, command);
    return this;
  }

  /**
   * Alias for 'onCommand'
   * @param {Resolver} cmd The comparator to test user commands against
   * @param {...Middleware} middleware The middleware functions to execute
   * @return {Router} this
   */
  cmd(cmd, ...middleware) {
    return this.onCommand(cmd, ...middleware);
  }

  /**
   * Removes a path, mounted Router or Command.
   * @param {Resolver} cmd The mount to delete.
   * @return {Router} this
   */
  offCommand(cmd) {
    this.mounts.delete(cmd);
    return this;
  }

  /**
   * Adds middleware which is always executed once reached. The middleware
   * cannot be unmounted using 'off'.
   * @param {...Middleware} middleware The middleware functions to execute
   * @return {Router} this
   */
  middleware(...middleware) {
    var command = new Command();
    middleware.forEach(ware => command.chain(ware));
    this.mounts.set(Symbol(), command);
    return this;
  }

  /**
   * Constructs and returns a new Router mounted on the given path. Basically
   * inline `mount(cmd, new Router());`
   * @param {Resolver} cmd The comparator to test user commands against
   * @param {...Middleware} middleware The middleware functions to execute
   * @return {Router} The router mounted on the path ${cmd}
   */
  group(cmd, ...middleware) {
    if (typeof cmd === 'function') {
      middleware.unshift(cmd);
      cmd = '';
    }
    var router = new Router();
    this.mount(cmd, router, ...middleware);
    return router;
  }

  /**
   * Mounts a router on the given path (like app.use(express.Router) in express)
   * Note that deferred resolution statuses only chain once - sub-sub-routers
   * won't cause a deferredResolution status in this router.
   * @param {Resolver} [cmd] The comparator to test user commands against
   * @param {Router} router the router to mount
   * @param {...Middleware} middleware pre-router middleware functions
   * @return {Router} this
   */
  mount(cmd, router, ...middleware) {
    if (cmd instanceof Router) {
      if (router) middleware.unshift(router);
      router = cmd;
      cmd = '';
    }
    if (!(router instanceof Router)) {
      throw new Error("Expected router as second argument");
    }
    var command = new Command(cmd);
    middleware.forEach(ware => command.chain(ware));
    command.chain(function() { return router.resolve(...arguments) });
    this.mounts.set(cmd || command, command);
    return this;
  }

  /**
   * Registers a listener for a preResolution event and fires a callback if the
   * user command that triggered the event matches a given resolver.
   * pre-hooks are fired only if the command was already going to match.
   * These hooks are syncronous!
   * @param {Resolver} [cmd] The comparator to test user commands against
   * @param {Middleware} callback The callback given the original arguments
   * @return {Router} this
   */
  pre(cmd, callback) {
    if (typeof callback === 'undefined') {
      callback = cmd;
      cmd = null;
    }
    var customTester = new Command(cmd);
    this.on('preResolution', (router, command, args) => {
      if (router.test && router.test(command) === false) return;
      if (customTester.test(command) === false) return;
      if (callback(command, ...args) === false) {
        throw null;
      }
    });
    return this;
  }

  /**
   * Registers a listener for a postResolution event and fires a callback if the
   * user command that triggered the event matches a given resolver.
   * These hooks are syncronous!
   * @param {Resolver} [cmd] The comparator to test user commands against
   * @param {Function} callback The callback given original arguments.
   * @return {Router} this
   */
  post(cmd, callback) {
    if (typeof callback === 'undefined') {
      callback = cmd;
      cmd = null;
    }
    var customTester = new Command(cmd);
    this.on('postResolution', (router, command, args, retvalue) => {
      if (router.test && router.test(command) === false) return;
      if (customTester.test(command) === false) return;
      if (callback(command, ...args) === false) {
        throw null;
      }
    });
    return this;
  }

  /**
   * Tests, routes and executes a user command
   * @param {string} cmd The plaintext command to execute
   * @param {...Object} args Extra arguments to pass to middleware
   * @return {Promise} A promise that resolves when processing is done.
   * @emits resolutionSuccess when any command match is encountered.
   *        {Router|Command} the failed router
   *        {String} The plaintext user command
   *        {[Object]} The extra arguments passed to the object
   * @emits preResolution before a command is executed. Good place to throw an
   *                      exception to stop the command.
   *        {Router|Command} the failed router
   *        {String} The plaintext user command
   *        {[Object]} The extra arguments passed to the object
   * @emits postResolution after a command is executed. Good place to throw an
   *                      exception to stop the command.
   *        {Router|Command} the failed router
   *        {String} The plaintext user command
   *        {[Object]} The extra arguments passed to the object
   *        {Object} The return value of the command
   * @emits resolutionFailed when all commands fail to match.
   *        {String} The plaintext user command
   *        {[Object]} The extra arguments passed to the object
   * @emits resolutionException when an exception is thrown in a command
   *        NOTE: If there are no handlers, the exception will bubble up.
   *        {Router|Command} the router or command that threw the exception
   *        {Error} The exception
   *        {String} The plaintext user command
   *        {[Object]} The extra arguments passed to the object
   */
  resolve(cmd, ...args) {
    cmd = cmd.trim();
    //logger.debug(`Resolve router for '${cmd}'`);//` with arguments [${args.join(", ")}]`);
    const links = this.mounts.values();
    const router = this;

    var promise = Promise.resolve();
    var resolved = false;

    (function nextLink() {
      var { value, done } = links.next(); // 'done' is when the iterator ends
      if (done) return;
      promise = promise.then(async () => {
        router.emit("preResolution", value, cmd, args);
        const ret = value.resolve(cmd, ...args);
        router.emit("postResolution", value, cmd, args, ret);

        if (ret instanceof Promise) {
          ret.then(() => {
            router.emit("resolutionSuccess", value, cmd, args);
            resolved = true;
          });
        }

        return ret;
      }).catch(ex => {
        if (!ex) return; // No exception, fail silently.
        if (router.listenerCount("resolutionException") == 0)
          logger.error(ex);
        router.emit("resolutionException", value, ex, cmd, args);
      })
      nextLink();
    })();

    return promise.then(() => {
      // If nothing resolved by the time the promise chain resolved,
      // nothing is going to resolve and we can fire 'resolutionFailed'
      if (!resolved) {
        router.emit("resolutionFailed", cmd, args);
        return Promise.reject();
      }
    });
  }
}
util.inherits(Router, EventEmitter);
module.exports = Router;
