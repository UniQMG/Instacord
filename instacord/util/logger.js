const abbreviate = require('./abbreviate.js');
const chalk = require('chalk');
const util = require('util');

const streams = {
 critical: [],
 error:    [],
 warning:  [],
 info:     [],
 debug:    []
}
const streamPriority = {
 critical: 0,
 error:    1,
 warning:  2,
 info:     3,
 debug:    4
}
const streamColors = {
 critical: (arg) => chalk.red(arg) + " [CRITICAL]",
 error:    chalk.red,
 warning:  chalk.yellow,
 info:     chalk.white,
 debug:    chalk.blue
}

class Logger {
  /**
   * Creates a new logger.
   * @param {string} domain the logger name, included in the logs.
   */
  constructor(domain) {
    this.domain = domain;
    this.shortDomain = abbreviate(domain);
  }

  /**
   * Adds a logging stream which is written to when the specified level is used.
   * Default levels include 'critical', 'error', 'warning', 'info', 'debug'
   * All loggers share the same pool of streams!
   * @param {string} level The minimum logging level to write to this stream
   * @param {function} write A function that accepts written logs
   */
  addStream(level, write) {
    if (!streams[level]) throw new Error("Unknown stream level: " + level);
    streams[level].push(write);
  }

  /**
   * Logs on a specific level.
   * Default levels include 'critical', 'error', 'warning', 'info', 'debug'
   * If no logger streams are availible on a given level, lower-level streams
   * will be used instead. So, errors would be printed to the info stream.
   * If no streams at all are availible, output defaults to console.log,
   * except for 'debug' level. 'debug' level logging is ignored with no streams.
   * @param {string} level Sets the logging level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  log(level, ...logs) {
    if (!streams[level]) throw new Error("Unknown stream level: " + level);
    var priority = streamPriority[level];
    var streamColor = streamColors[level];
    var written = false;

    Object.keys(streams).filter(streamName => {
      return streamPriority[streamName] >= priority
    }).map(stream => {
      var streamable = streams[stream];
      if (streamable.length == 0 && stream == 'info')
        streamable = [ console.log ];
      return [streamable, stream];
    }).forEach(([stream, streamName]) => {
      if (written) return;
      stream.forEach(writeFunction => {
        writeFunction(`${streamColor(`[${this.shortDomain}]`)} ${logs.map(log => {
          if (typeof(log) == 'string') return log;
          return util.inspect(log);
        }).join(" ")}`);
        written = true;
      });
    })
  }

  /**
   * Logs on the 'debug' level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  debug(...logs) { this.log('debug', ...logs); }

  /**
   * Logs on the 'info' level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  info(...logs) { this.log('info', ...logs); }

  /**
   * Logs on the 'warning' level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  warn(...logs) { this.log('warning', ...logs); }

  /**
   * Logs on the 'warning' level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  warning(...logs) { this.log('warning', ...logs); }

  /**
   * Logs on the 'error' level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  error(...logs) { this.log('error', ...logs); }

  /**
   * Logs on the 'critical' level.
   * @param {...Object} logs Things to write to logfile. Probably strings.
   */
  critical(...logs) { this.log('critical', ...logs); }
}
module.exports = Logger;
