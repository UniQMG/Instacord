module.exports = function(thing) {
  try {
    return require(thing);
  } catch(ex) {
    return null;
  }
}
